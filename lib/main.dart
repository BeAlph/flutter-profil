import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage()
    );
  }
}

class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.green,
        padding: EdgeInsets.all(5.0),
        child: Container(
          color: Colors.blueGrey,
          padding: EdgeInsets.all(8.0),
          child: Container(
            color: Colors.white,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 20,
                                ),
                                SizedBox(
                                  child: CircleAvatar(
                                    backgroundImage:
                                    AssetImage('images/ont.jpg'),
                                  ),
                                  width: 120,
                                  height: 120,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                          'Halo',

                                      ),
                                      Text(
                                          'Nama Saya Fathir RD',
                                          style: TextStyle(fontSize: 15)
                                      ),
                                      Text(
                                        'NPM 1715061013',
                                          style: TextStyle(fontSize: 15)
                                      ),
                                      Text(
                                          'Lahir di Talangpadang, 22 Mei 1998',
                                          style: TextStyle(fontSize: 12)
                                      ),
                                      Text(
                                          'fathir.rahman1013@students.unila.ac.id',
                                          style: TextStyle(fontSize: 12)
                                      ),
                                      Text(
                                          'Tinggal di Jl. Untung Suropati',
                                          style: TextStyle(fontSize: 12)
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}
